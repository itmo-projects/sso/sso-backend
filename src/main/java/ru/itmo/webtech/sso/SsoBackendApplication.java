package ru.itmo.webtech.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SsoBackendApplication.class, args);
	}

}
