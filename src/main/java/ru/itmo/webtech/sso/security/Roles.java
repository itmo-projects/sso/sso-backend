package ru.itmo.webtech.sso.security;

public enum Roles {


    ADMIN("app_admin"),
    USER("app_user");

    private final String role;
    Roles(String role) {
        this.role = role;
    }

    public String role() {
        return role;
    }
}
