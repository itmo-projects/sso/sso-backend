package ru.itmo.webtech.sso.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(
        origins = {"*"},
        allowedHeaders = {"*"},
        methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE,
                RequestMethod.OPTIONS, RequestMethod.HEAD}
)
@RestController
@RequestMapping("/test")
public class SecuredResource {
    public static final String USERNAME_ATTRIBUTE = "preferred_username";

    @GetMapping("/unsecured")
    public ResponseEntity<ResponceStructure> getUnSecured() {
        return ResponseEntity.ok(new ResponceStructure("Unsecured Resource"));
    }

    @GetMapping("/admin")
    public ResponseEntity<ResponceStructure> getAdmin(Principal principal) {
        JwtAuthenticationToken token = (JwtAuthenticationToken) principal;
        String userName = (String) token.getTokenAttributes().get(USERNAME_ATTRIBUTE);

        Collection<? extends GrantedAuthority> roles = ((JwtAuthenticationToken) principal).getAuthorities();

        ResponceStructure jsonStructure = new ResponceStructure(userName, roles);

        return ResponseEntity.ok(jsonStructure);
    }

    @GetMapping("/user")
    public ResponseEntity<ResponceStructure> getUser(Principal principal) {
        JwtAuthenticationToken token = (JwtAuthenticationToken) principal;
        String userName = (String) token.getTokenAttributes().get(USERNAME_ATTRIBUTE);

        Set<String> applicationRoles = ((JwtAuthenticationToken) principal)
                .getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .filter(authority -> authority.startsWith("ROLE_app"))
                .collect(Collectors.toSet());

        ResponceStructure jsonStructure = new ResponceStructure(userName, applicationRoles);

        return ResponseEntity.ok(jsonStructure);
    }
}
