package ru.itmo.webtech.sso.resource;

import java.util.Collection;

public class ResponceStructure {

    private final String userName;
    public Collection<?> userRoles;


    public ResponceStructure(String userName) {
        this.userName = userName;
    }

    public ResponceStructure(String userName, Collection<?> userRoles) {
        this.userName = userName;
        this.userRoles = userRoles;
    }

    public String userName() {
        return userName;
    }

    public Collection<?> userRoles() {
        return userRoles;
    }
}

