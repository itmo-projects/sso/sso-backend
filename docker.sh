#!/bin/zsh

mvn clean install

docker container rm -f sso-backend

docker image rm -f sso-backend:latest

docker build --tag sso-backend:latest .

docker run --rm -e KEYCLOAK_URL="http://172.17.0.2:8180" -e REALM="sso-itmo" -d -p 9000:9000 --name sso-backend sso-backend:latest


docker run --rm -e KEYCLOAK_URL="http://localhost:8180" -e REALM="sso-itmo" -d -p 9000:9000 --name sso-backend sso-backend:latest