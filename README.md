# Backend для тестирования SSO


registry.gitlab.com/itmo-projects/sso/sso-backend:v0.3


export KEYCLOAK_URL=http://179.43.151.18:8180/ &&
export KEYCLOAK_REALM=sso-itmo &&
export CI_IMAGE_BACKEND_TAG=v0.3

docker run -e KEYCLOAK_URL=$KEYCLOAK_URL \
-e KEYCLOAK_REALM=$KEYCLOAK_REALM \
-d -p 9000:9000 --name sso-backend \
registry.gitlab.com/itmo-projects/sso/sso-backend:$CI_IMAGE_BACKEND_TAG

