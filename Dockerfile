FROM openjdk:17

ENV TZ=Europe/Moscow

COPY /sso-backend*.jar /sso-backend.jar

EXPOSE 9000

ENTRYPOINT [ "java", 					    			\
             "-XX:+ExitOnOutOfMemoryError", 			\
             "-XX:InitialRAMPercentage=25", 			\
             "-XX:MaxRAMPercentage=50",   			    \
             "-Duser.timezone=$TZ",     			    \
             "-jar",           						    \
             "/sso-backend.jar"                         \
]